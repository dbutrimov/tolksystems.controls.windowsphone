﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Data;
using System.Linq;

namespace TolkSystems.Controls.WindowsPhone
{
    public static class ControlUtils
    {
		//public static FrameworkElement FindChild(FrameworkElement obj, string name, bool recursively = true)
		//{
		//	int count = VisualTreeHelper.GetChildrenCount(obj);
		//	for (int index = 0; index < count; index++)
		//	{
		//		FrameworkElement child = VisualTreeHelper.GetChild(obj, index) as FrameworkElement;
		//		if (child != null)
		//		{
		//			if (child.Name == name)
		//				return child;
		//			else if (recursively)
		//			{
		//				FrameworkElement res = FindChild(child, name, recursively);
		//				if (res != null)
		//					return res;
		//			}
		//		}
		//	}

		//	return null;
		//}


		public static T FindInParents<T>(this UIElement element)
			where T : UIElement
		{
			if (element == null)
				throw new ArgumentNullException("element");

			var current = (DependencyObject)element;
			while (current != null)
			{
				current = VisualTreeHelper.GetParent(current);
				var found = current as T;
				if (found != null)
					return found;
			}

			return null;
		}


        public static Control GetNextTabStopControl(FrameworkElement root, int currentTabIndex, IEnumerable<Type> types = null)
        {
            Control result = null;

            var childCount = VisualTreeHelper.GetChildrenCount(root);
            for (int i = 0; i < childCount; i++)
            {
                var child = VisualTreeHelper.GetChild(root, i) as Control;
                if ((child != null) && (types == null || types.Contains(child.GetType())))
                {
                    if ((child.TabIndex > currentTabIndex) && (result == null || child.TabIndex < result.TabIndex))
                    {
                        result = child;
                    }
                }
            }

            return result;
        }
    }
}
