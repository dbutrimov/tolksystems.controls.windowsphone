﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace TolkSystems.Controls.WindowsPhone
{
	public class AlphaContainer : ContentControl
	{
		private Brush _opacityMask = null;

		private FrameworkElement _rootLayout = null;
		private ContentPresenter _contentPresenter = null;

		public AlphaContainer()
		{
			DefaultStyleKey = typeof(AlphaContainer);

		}

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();


			_rootLayout = GetTemplateChild("PART_RootLayout") as FrameworkElement;
			_opacityMask = _rootLayout.OpacityMask;
			_rootLayout.SizeChanged += RootLayout_SizeChanged;

			_contentPresenter = GetTemplateChild("PART_ContentPresenter") as ContentPresenter;
			_contentPresenter.SizeChanged += ContentPresenter_SizeChanged;

			UpdateMask();
		}

		void RootLayout_SizeChanged(object sender, System.Windows.SizeChangedEventArgs e)
		{
			UpdateMask();
		}

		void ContentPresenter_SizeChanged(object sender, System.Windows.SizeChangedEventArgs e)
		{
			UpdateMask();
		}

		void UpdateMask()
		{
			//Debug.WriteLine("{0} {1}", _contentPresenter.ActualWidth, _rootLayout.ActualWidth);

			if (_contentPresenter != null && _rootLayout != null
				&& _contentPresenter.ActualWidth >= _rootLayout.ActualWidth)
			{
				_rootLayout.OpacityMask = _opacityMask;
			}
			else
			{
				_rootLayout.OpacityMask = null;
			}
		}
	}
}
