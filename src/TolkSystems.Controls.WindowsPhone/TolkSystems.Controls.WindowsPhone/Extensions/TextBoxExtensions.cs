﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace TolkSystems.Controls.WindowsPhone
{
    public static class TextBoxExtensions
    {
        public static bool HideKeyboard()
        {
			var root = Application.Current.RootVisual as Control;
			if (root != null)
				return root.Focus();
			
			return false;
        }

        public static bool HideKeyboard(this TextBox textBox)
        {
            return HideKeyboard();
        }
    }
}
