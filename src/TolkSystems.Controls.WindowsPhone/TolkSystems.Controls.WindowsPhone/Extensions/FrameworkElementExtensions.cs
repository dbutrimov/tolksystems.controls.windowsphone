﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace TolkSystems.Controls.WindowsPhone
{
	public static class FrameworkElementExtensions
	{
		/// Listen for change of the dependency property  
		public static void RegisterForNotification(this FrameworkElement element, string propertyName, PropertyChangedCallback callback)
		{
			//Bind to a depedency property  
			var b = new Binding(propertyName) { Source = element };
			var prop = DependencyProperty.RegisterAttached(
				"ListenAttached_" + propertyName,
				typeof(object),
				element.GetType(),
				new PropertyMetadata(callback));
			element.SetBinding(prop, b);
		}
	}
}
