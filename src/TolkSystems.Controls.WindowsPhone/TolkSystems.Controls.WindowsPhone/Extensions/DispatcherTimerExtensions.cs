﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Threading;

namespace TolkSystems.Controls.WindowsPhone
{
	public static class DispatcherTimerExtensions
    {
        private class InnerDispatcherTimer : DispatcherTimer
		{
			private SendOrPostCallback _callback;
			private object _userState;

			public InnerDispatcherTimer(SendOrPostCallback callback, object userState)
			{
				if (callback == null)
					throw new ArgumentNullException("callback");

				_callback = callback;
				_userState = userState;
			}

			public void Invoke()
			{
				_callback(_userState);
			}
        }

        public static void SingleShot(TimeSpan interval, SendOrPostCallback callback, object state)
        {
            var timer = new InnerDispatcherTimer(callback, state);
            timer.Interval = interval;
			timer.Tick += SingleShot_Tick;

            timer.Start();
        }

		private static void SingleShot_Tick(object sender, EventArgs e)
		{
			var t = (InnerDispatcherTimer)sender;
			t.Tick -= SingleShot_Tick;
			t.Stop();

			t.Invoke();
		}
    }
}
