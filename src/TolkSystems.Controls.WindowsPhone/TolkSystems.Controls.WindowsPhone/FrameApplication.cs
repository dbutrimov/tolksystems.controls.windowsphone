﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace TolkSystems.Controls.WindowsPhone
{
	public class FrameApplication : Application, IFrameApplication
	{
		private PhoneApplicationFrame _rootFrame;
		public PhoneApplicationFrame RootFrame 
		{
			get { return _rootFrame; }
			set
			{
				if (value != _rootFrame)
				{
					var old = _rootFrame;
					_rootFrame = value;
					OnRootFrameChanged(new RoutedPropertyChangedEventArgs<PhoneApplicationFrame>(old, _rootFrame));
				}
			}
		}

		public event RoutedPropertyChangedEventHandler<PhoneApplicationFrame> RootFrameChanged;

		protected virtual void OnRootFrameChanged(RoutedPropertyChangedEventArgs<PhoneApplicationFrame> e)
		{
			
		}
	}
}
