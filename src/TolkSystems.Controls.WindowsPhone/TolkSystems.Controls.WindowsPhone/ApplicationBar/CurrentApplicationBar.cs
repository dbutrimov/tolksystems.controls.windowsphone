﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace TolkSystems.Controls.WindowsPhone
{
	public static class CurrentApplicationBar
	{
		static CurrentApplicationBar()
		{
			var app = (IFrameApplication)Application.Current;
			if (app.RootFrame != null)
				OnApplicationFrameChanged(null, app.RootFrame);
			app.RootFrameChanged += Application_RootFrameChanged;
		}

		private static void Application_RootFrameChanged(object sender, RoutedPropertyChangedEventArgs<PhoneApplicationFrame> e)
		{
			OnApplicationFrameChanged(e.OldValue, e.NewValue);
		}

		private static void OnApplicationFrameChanged(object oldValue, object newValue)
		{
			var appBar = Current;

			if (oldValue != null)
			{
				var contentChanged = (IContentChanged)oldValue;
				contentChanged.ContentChanged -= RootFrame_ContentChanged;

				var page = (ApplicationPage)contentChanged.Content;
				if (page != null)
				{
					page.ApplicationBarChanged -= ApplicationPage_ApplicationBarChanged;
					appBar = null;
				}
			}

			if (newValue != null)
			{
				var contentChanged = (IContentChanged)newValue;
				contentChanged.ContentChanged += RootFrame_ContentChanged;

				var page = (ApplicationPage)contentChanged.Content;
				if (page != null)
				{
					appBar = page.ApplicationBar;
					page.ApplicationBarChanged += ApplicationPage_ApplicationBarChanged;
				}
			}

			Current = appBar;
		}

		private static void RootFrame_ContentChanged(object sender, RoutedPropertyChangedEventArgs e)
		{
			var appBar = Current;

			var oldValue = (ApplicationPage)e.OldValue;
			var newValue = (ApplicationPage)e.NewValue;

			if (oldValue != null)
			{
				oldValue.ApplicationBarChanged -= ApplicationPage_ApplicationBarChanged;
				appBar = null;
			}

			if (newValue != null)
			{
				appBar = newValue.ApplicationBar;
				newValue.ApplicationBarChanged += ApplicationPage_ApplicationBarChanged;
			}

			Current = appBar;
		}

		private static void ApplicationPage_ApplicationBarChanged(object sender, RoutedPropertyChangedEventArgs<IApplicationBar> e)
		{
			Current = e.NewValue;
		}






		private static IApplicationBar _current = null;
		public static IApplicationBar Current
		{
			get { return _current; }
			private set
			{
				if (value != _current)
				{
					var old = _current;
					_current = value;
					OnCurrentChanged(new RoutedPropertyChangedEventArgs<IApplicationBar>(old, _current));
				}
			}
		}

		public static event RoutedPropertyChangedEventHandler<IApplicationBar> CurrentChanged;
		private static void OnCurrentChanged(RoutedPropertyChangedEventArgs<IApplicationBar> e)
		{
			if (CurrentChanged != null)
				CurrentChanged(null, e);
		}
	}
}
