﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Shell;
using System.Collections.Generic;
using System.Windows.Threading;
using Microsoft.Phone.Controls;

namespace TolkSystems.Controls.WindowsPhone
{
    public static class ApplicationBarLocker
	{
		private static int _lockCounter = 0;
		private static IApplicationBar _appBar;

		static ApplicationBarLocker()
		{
			CurrentApplicationBar.CurrentChanged += CurrentApplicationBar_CurrentChanged;
		}

		private static void CurrentApplicationBar_CurrentChanged(object sender, RoutedPropertyChangedEventArgs<IApplicationBar> e)
		{
			if (_lockCounter > 0)
			{
				if (_appBar != null)
					_appBar.IsVisible = true;

				_appBar = e.NewValue;
				if (_appBar != null)
					_appBar.IsVisible = false;
			}
		}


        public static void Lock()
        {
			if (_lockCounter <= 0)
			{
				_appBar = CurrentApplicationBar.Current;
				if (_appBar != null)
					_appBar.IsVisible = false;
			}

			_lockCounter++;
        }

		public static void Unlock()
		{
			if (_lockCounter <= 0)
				return;

			_lockCounter--;
			if (_lockCounter <= 0)
			{
				if (_appBar != null)
				{
					_appBar.IsVisible = true;
					_appBar = null;
				}
			}
		}

		public static void Reset()
		{
			_lockCounter = 0;
			if (_appBar != null)
			{
				_appBar.IsVisible = true;
				_appBar = null;
			}
		}
    }
}
