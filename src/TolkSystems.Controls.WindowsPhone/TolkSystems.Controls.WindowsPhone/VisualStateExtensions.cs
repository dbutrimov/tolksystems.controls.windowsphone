﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace TolkSystems.Controls.WindowsPhone
{
	public static class VisualStateExtensions
	{
		#region Storyboard Completed
		private class CompletedCallback
		{
			private SendOrPostCallback _callback;
			private object _state;

			public CompletedCallback(SendOrPostCallback callback, object state)
			{
				if (callback == null)
					throw new ArgumentNullException("callback");

				_callback = callback;
				_state = state;
			}

			public void Invoke()
			{
				_callback(_state);
			}
		}

		private static Dictionary<Storyboard, CompletedCallback> _completedActions = new Dictionary<Storyboard, CompletedCallback>();

		private static CompletedCallback TakeCompletedCallback(Storyboard storyboard)
		{
			CompletedCallback callback = null;
			if (_completedActions.TryGetValue(storyboard, out callback))
				_completedActions.Remove(storyboard);

			return callback;
		}

		private static void SetCompletedCallback(Storyboard storyboard, CompletedCallback callback)
		{
			storyboard.Completed += new EventHandler(Storyboard_Completed);
			_completedActions[storyboard] = callback;
		}

		private static void SetCompletedCallback(Storyboard storyboard, SendOrPostCallback callback, object state)
		{
			storyboard.Completed += new EventHandler(Storyboard_Completed);
			_completedActions[storyboard] = new CompletedCallback(callback, state);
		}

		private static void Storyboard_Completed(object sender, EventArgs e)
		{
			Storyboard storyboard = (Storyboard)sender;
			storyboard.Completed -= Storyboard_Completed;

			var callback = TakeCompletedCallback(storyboard);
			if (callback != null)
				callback.Invoke();
		}
		#endregion

		public static bool GoToVisualState(this FrameworkElement obj, string groupName, string stateName, 
			SendOrPostCallback callback = null, object userState = null, bool recursively = true)
		{
			foreach (VisualStateGroup group in VisualStateManager.GetVisualStateGroups(obj))
			{
				if (group.Name == groupName)
				{
					foreach (VisualState state in group.States)
					{
						if (state.Name == stateName)
						{
							if (callback != null)
								SetCompletedCallback(state.Storyboard, callback, userState);
							state.Storyboard.Begin();
							return true;
						}
					}
				}
			}

			if (recursively)
			{
				int count = VisualTreeHelper.GetChildrenCount(obj);
				for (int index = 0; index < count; index++)
				{
					FrameworkElement child = VisualTreeHelper.GetChild(obj, index) as FrameworkElement;
					if (child != null && GoToVisualState(child, groupName, stateName, callback, userState, recursively))
						return true;
				}
			}

			return false;
		}
	}
}
