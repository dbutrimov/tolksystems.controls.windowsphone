﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Diagnostics;
using System.Windows.Navigation;
using Microsoft.Phone.Shell;
using System.ComponentModel;
using System.Windows.Threading;

namespace TolkSystems.Controls.WindowsPhone
{
	//public class ValueChangedEventArgs<T> : EventArgs
	//{
	//	public T OldValue { get; set; }
	//	public T NewValue { get; set; }
	//}

	//public class HandledEventArgs : EventArgs
	//{
	//	public bool Handled { get; set; }

	//	public HandledEventArgs()
	//	{
	//		Handled = false;
	//	}
	//}

    //public delegate void ValueChangedEventHandler<T>(object sender, ValueChangedEventArgs<T> e);

    //public enum TransitionMode { None, Turnstile, Slide };

    public class ApplicationPage : PhoneApplicationPage
    {
        #region SystemTrayForegroundColor
        public static readonly DependencyProperty SystemTrayForegroundColorProperty =
            DependencyProperty.Register("SystemTrayForegroundColorProperty", typeof(Color),
            typeof(ApplicationPage), new PropertyMetadata((Color)Application.Current.Resources["PhoneForegroundColor"], 
                OnSystemTrayForegroundColorPropertyChanged));

        public Color SystemTrayForegroundColor
        {
            get { return (Color)GetValue(SystemTrayForegroundColorProperty); }
            set { SetValue(SystemTrayForegroundColorProperty, value); }
        }

        private static void OnSystemTrayForegroundColorPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var page = (ApplicationPage)d;

            //if (page._isLoaded)
            //    SystemTray.ForegroundColor = (Color)e.NewValue;
			SystemTray.SetForegroundColor(page, (Color)e.NewValue);
        }
        #endregion


		//#region NavigationInTransition DependencyProperty
		//public TransitionMode NavigationInTransition 
		//{
		//	get { return (TransitionMode)GetValue(NavigationInTransitionProperty); }
		//	set { SetValue(NavigationInTransitionProperty, value); }
		//}

		//public static readonly DependencyProperty NavigationInTransitionProperty =
		//	DependencyProperty.Register("NavigationInTransition", typeof(TransitionMode), typeof(ApplicationPage),
		//	new PropertyMetadata(TransitionMode.None, NavigationInTransitionChanged));

		//private static void NavigationInTransitionChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		//{
		//	ApplicationPage sender = (ApplicationPage)obj;

		//	//NavigationInTransition inTransition = TransitionService.GetNavigationInTransition(sender);
		//	//switch ((TransitionMode)e.NewValue)
		//	//{
		//	//	case TransitionMode.Slide:
		//	//		inTransition.Backward = new SlideTransition() { Mode = SlideTransitionMode.SlideRightFadeIn };
		//	//		inTransition.Forward = new SlideTransition() { Mode = SlideTransitionMode.SlideLeftFadeIn };
		//	//		break;

		//	//	case TransitionMode.Turnstile:
		//	//		inTransition.Backward = new TurnstileTransition() { Mode = TurnstileTransitionMode.BackwardIn };
		//	//		inTransition.Forward = new TurnstileTransition() { Mode = TurnstileTransitionMode.ForwardIn };
		//	//		break;

		//	//	default:
		//	//		inTransition.Backward = null;
		//	//		inTransition.Forward = null;
		//	//		break;
		//	//}
		//}
		//#endregion

		//#region NavigationOutTransition DependencyProperty
		//public TransitionMode NavigationOutTransition
		//{
		//	get { return (TransitionMode)GetValue(NavigationOutTransitionProperty); }
		//	set { SetValue(NavigationOutTransitionProperty, value); }
		//}

		//public static readonly DependencyProperty NavigationOutTransitionProperty =
		//	DependencyProperty.Register("NavigationOutTransition", typeof(TransitionMode), typeof(ApplicationPage),
		//	new PropertyMetadata(TransitionMode.None, NavigationOutTransitionChanged));

		//private static void NavigationOutTransitionChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		//{
		//	ApplicationPage sender = (ApplicationPage)obj;

		//	//NavigationOutTransition outTransition = TransitionService.GetNavigationOutTransition(sender);
		//	//switch ((TransitionMode)e.NewValue)
		//	//{
		//	//	case TransitionMode.Slide:
		//	//		outTransition.Backward = new SlideTransition() { Mode = SlideTransitionMode.SlideRightFadeOut };
		//	//		outTransition.Forward = new SlideTransition() { Mode = SlideTransitionMode.SlideLeftFadeOut };
		//	//		break;

		//	//	case TransitionMode.Turnstile:
		//	//		outTransition.Backward = new TurnstileTransition() { Mode = TurnstileTransitionMode.BackwardOut };
		//	//		outTransition.Forward = new TurnstileTransition() { Mode = TurnstileTransitionMode.ForwardOut };
		//	//		break;

		//	//	default:
		//	//		outTransition.Backward = null;
		//	//		outTransition.Forward = null;
		//	//		break;
		//	//}
		//}
		//#endregion

		////public bool UseTombstoneHelper { get; set; }

		//private bool _isLoaded = false;

        public ApplicationPage()
        {
            this.RegisterForNotification("ApplicationBar", (d, e) => OnApplicationBarChanged(e.OldValue, e.NewValue));
                        
			//InitializeNavigationTransition();

			//Loaded += Page_Loaded;
			//Unloaded += Page_Unloaded;
        }


		public event RoutedPropertyChangedEventHandler<IApplicationBar> ApplicationBarChanged;
        protected virtual void OnApplicationBarChanged(object oldValue, object newValue)
        {
            if (ApplicationBarChanged != null)
            {
				ApplicationBarChanged(this,
					new RoutedPropertyChangedEventArgs<IApplicationBar>(
						(IApplicationBar)oldValue, (IApplicationBar)newValue));
            }
        }


		//private void Page_Loaded(object sender, RoutedEventArgs e)
		//{
		//	_isLoaded = true;

		//	//SystemTray.ForegroundColor = SystemTrayForegroundColor; 

		//	//Loaded -= OnPageLoaded;
		//	OnLoaded();
		//}

		//private void Page_Unloaded(object sender, RoutedEventArgs e)
		//{
		//	_isLoaded = false;
		//}

		//protected virtual void OnLoaded()
		//{
            
		//}

		//private void InitializeNavigationTransition()
		//{
		//	NavigationInTransition inTransition = new NavigationInTransition();
		//	TransitionService.SetNavigationInTransition(this, inTransition);

		//	NavigationOutTransition outTransition = new NavigationOutTransition();
		//	TransitionService.SetNavigationOutTransition(this, outTransition);
		//}


        private bool _tombstoned = true;
        
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

			SystemTray.SetForegroundColor(this, SystemTrayForegroundColor); 

			if (e.NavigationMode == NavigationMode.New)
			{
				OnNavigatePage();
			}
			else if (e.NavigationMode == NavigationMode.Back && _tombstoned)
			{
				OnRestorePage();
			}

			_tombstoned = false;



            /*if (UseTombstoneHelper)
            {
                if (e.NavigationMode == NavigationMode.Back && _tombstone)
                {
                    HandledEventArgs he = new HandledEventArgs();
                    OnRestoreState(he);

                    if (!he.Handled)
                        TombstoneHelper.RestoreState(this);

                    OnStateRestored();
                }

                _tombstone = false;
            }*/

            //DispatcherTimerExtensions.SingleShot(TimeSpan.FromMilliseconds(300), OnNavigated);
        }

		//protected virtual void OnNavigated()
		//{

		//}

		protected virtual void OnRestorePage()
		{

		}

		protected virtual void OnNavigatePage()
		{

		}

		//protected override void OnNavigatedFrom(NavigationEventArgs e)
		//{
		//	base.OnNavigatedFrom(e);

		//	/*if (UseTombstoneHelper)
		//	{
		//		if (e.NavigationMode != NavigationMode.Back)
		//		{
		//			HandledEventArgs he = new HandledEventArgs();
		//			OnSaveState(he);

		//			if (!he.Handled)
		//				TombstoneHelper.SaveState(this);
		//		}
		//	}*/
		//}

		//protected virtual void OnSaveState(HandledEventArgs e)
		//{
		//	Debug.WriteLine("Saving page state...");
		//}

		//protected virtual void OnRestoreState(HandledEventArgs e)
		//{
		//	Debug.WriteLine("Loading page state...");
		//}

		//protected virtual void OnStateRestored()
		//{
		//}
    }
}
