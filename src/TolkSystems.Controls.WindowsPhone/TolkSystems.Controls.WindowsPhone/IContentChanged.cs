﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace TolkSystems.Controls.WindowsPhone
{
	public class RoutedPropertyChangedEventArgs : RoutedPropertyChangedEventArgs<object> 
	{
		public RoutedPropertyChangedEventArgs(object oldValue, object newValue)
			: base(oldValue, newValue)
		{

		}
	
	}

	public delegate void RoutedPropertyChangedEventHandler(object sender, RoutedPropertyChangedEventArgs e);

	public interface IContentChanged
	{
		object Content { get; }
		event RoutedPropertyChangedEventHandler ContentChanged;
	}
}
