﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Shell;

namespace TolkSystems.Controls.WindowsPhone
{
    public class Pivot : Microsoft.Phone.Controls.Pivot
    {
        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            base.OnSelectionChanged(e);

            var item = SelectedItem as PivotItem;
            SystemTray.ProgressIndicator = (item != null) ? item.ProgressIndicator : null;

            var rootFrame = (Frame)Application.Current.RootVisual;
            var page = (Microsoft.Phone.Controls.PhoneApplicationPage)rootFrame.Content;
            page.ApplicationBar = (item != null) ? item.ApplicationBar : null;
        }
    }
}
