﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Shell;

namespace TolkSystems.Controls.WindowsPhone
{
    public class PivotItem : Microsoft.Phone.Controls.PivotItem
    {
        #region ApplicationBar DependencyProperty

        public IApplicationBar ApplicationBar
        {
			get { return (IApplicationBar)GetValue(ApplicationBarProperty); }
            set { SetValue(ApplicationBarProperty, value); }
        }

        public static readonly DependencyProperty ApplicationBarProperty =
			DependencyProperty.Register("ApplicationBar", typeof(IApplicationBar), typeof(PivotItem),
            new PropertyMetadata(null, ApplicationBarChanged));

        private static void ApplicationBarChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            PivotItem sender = (PivotItem)o;

        }

        #endregion

        #region ProgressIndicator DependencyProperty

        public ProgressIndicator ProgressIndicator
        {
            get { return (ProgressIndicator)GetValue(ProgressIndicatorProperty); }
            set { SetValue(ProgressIndicatorProperty, value); }
        }

        public static readonly DependencyProperty ProgressIndicatorProperty =
            DependencyProperty.Register("ProgressIndicator", typeof(ProgressIndicator), typeof(PivotItem),
            new PropertyMetadata(null, ProgressIndicatorChanged));

        private static void ProgressIndicatorChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            PivotItem sender = (PivotItem)o;

        }

        #endregion
    }
}
