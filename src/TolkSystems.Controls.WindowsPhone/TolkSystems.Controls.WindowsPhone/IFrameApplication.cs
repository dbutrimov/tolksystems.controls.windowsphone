﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace TolkSystems.Controls.WindowsPhone
{
	public interface IFrameApplication
	{
		PhoneApplicationFrame RootFrame { get; set; }
		event RoutedPropertyChangedEventHandler<PhoneApplicationFrame> RootFrameChanged;
	}
}
