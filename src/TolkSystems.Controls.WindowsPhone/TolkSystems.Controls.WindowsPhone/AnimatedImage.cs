﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;

namespace TolkSystems.Controls.WindowsPhone
{
	[TemplatePart(Name = "PART_Image", Type = typeof(Image))]
	public class AnimatedImage : Control
	{
		#region AnimatedImage DependencyProperty
		protected static AnimatedImage GetAnimatedImage(ImageSource imageSource)
		{
			return (AnimatedImage)imageSource.GetValue(AnimatedImageProperty);
		}

		protected static void SetAnimatedImage(ImageSource imageSource, AnimatedImage value)
		{
			imageSource.SetValue(AnimatedImageProperty, value);
		}

		protected static readonly DependencyProperty AnimatedImageProperty =
			DependencyProperty.Register("AnimatedImage", typeof(AnimatedImage), typeof(ImageSource),
			new PropertyMetadata(null, OnAnimatedImagePropertyChanged));

		private static void OnAnimatedImagePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			var sender = (ImageSource)obj;
			//sender.OnSourceChanged((ImageSource)e.OldValue, (ImageSource)e.NewValue);
		}
		#endregion

		#region IsLoaded DependencyProperty
		public static bool GetIsOpened(ImageSource imageSource)
		{
			return (bool)imageSource.GetValue(IsOpenedProperty);
		}

		public static void SetIsOpened(ImageSource imageSource, bool value)
		{
			imageSource.SetValue(IsOpenedProperty, value);
		}

		public static readonly DependencyProperty IsOpenedProperty =
			DependencyProperty.Register("IsOpened", typeof(bool), typeof(ImageSource),
			new PropertyMetadata(false, OnIsOpenedPropertyChanged));

		private static void OnIsOpenedPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			var sender = (ImageSource)obj;
			var img = GetAnimatedImage(sender);
			if (img != null)
				img.UpdateSource();
		}
		#endregion






		private Image _image = null;


		#region Source DependencyProperty
		public ImageSource Source
		{
			get { return (ImageSource)GetValue(SourceProperty); }
			set { SetValue(SourceProperty, value); }
		}

		public static readonly DependencyProperty SourceProperty =
			DependencyProperty.Register("Source", typeof(ImageSource), typeof(AnimatedImage),
			new PropertyMetadata(null, OnSourcePropertyChanged));

		private static void OnSourcePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			var sender = (AnimatedImage)obj;
			sender.OnSourceChanged((ImageSource)e.OldValue, (ImageSource)e.NewValue);
		}
		#endregion


		public AnimatedImage()
		{
			DefaultStyleKey = typeof(AnimatedImage);
		}


		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();

			if (_image != null)
				_image.ImageOpened -= Image_ImageOpened;

			_image = GetTemplateChild("PART_Image") as Image;
			if (_image != null)
				_image.ImageOpened += Image_ImageOpened;

			UpdateSource();
		}


		protected virtual void OnSourceChanged(ImageSource oldValue, ImageSource newValue)
		{
			if (oldValue != null)
				AnimatedImage.SetAnimatedImage(oldValue, null);

			if (newValue != null)
				AnimatedImage.SetAnimatedImage(newValue, this);

			UpdateSource();
		}

		private void UpdateSource()
		{
			if (_image != null)
				_image.Source = Source;

			VisualStateManager.GoToState(this, (Source != null && GetIsOpened(Source)) ? "ImageLoadedState" : "ImageEmptyState", true);
		}


		private void Image_ImageOpened(object sender, RoutedEventArgs e)
		{
			if (Source != null)
				SetIsOpened(Source, true);
		}
	}
}
