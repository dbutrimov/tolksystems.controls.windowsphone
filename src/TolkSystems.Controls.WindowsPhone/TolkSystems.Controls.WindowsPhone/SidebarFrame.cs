﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Telerik.Windows.Controls;

namespace TolkSystems.Controls.WindowsPhone
{
	[TemplatePart(Name = "PART_Container", Type = typeof(Grid))]
	[TemplatePart(Name = "PART_ContainerTransform", Type = typeof(CompositeTransform))]
	[TemplatePart(Name = "PART_SidebarContainer", Type = typeof(Grid))]
	[TemplatePart(Name = "PART_SidebarContainerTransform", Type = typeof(CompositeTransform))]
    [TemplatePart(Name = "PART_ClientArea", Type = typeof(FrameworkElement))]
	public class SidebarFrame : PopupFrame
	{
		private const double SIDEBAR_WIDTH = 380d;

		private bool _isAppBarLocked;

		private Grid _container;
		private CompositeTransform _containerTransform;

		private Grid _sidebarContainer;
		private CompositeTransform _sidebarContainerTransform;

		private FrameworkElement _clientArea;

		#region DependencyProperties

		#region SidebarContent Property
		public static DependencyProperty SidebarContentProperty = DependencyProperty.Register(
			"SidebarContent", typeof(object), typeof(SidebarFrame), null);

		/// <summary>
		/// Gets or sets the content of a sidebar
		/// </summary>
		public object SidebarContent
		{
			get { return GetValue(SidebarContentProperty); }
			set { SetValue(SidebarContentProperty, value); }
		}
		#endregion

		#region SidebarTemplate Property
		public static DependencyProperty SidebarTemplateProperty = DependencyProperty.Register(
			"SidebarTemplate", typeof(DataTemplate), typeof(SidebarFrame), null);

		/// <summary>
		/// Gets or sets the content of a sidebar
		/// </summary>
		public DataTemplate SidebarTemplate
		{
			get { return (DataTemplate)GetValue(SidebarTemplateProperty); }
			set { SetValue(SidebarTemplateProperty, value); }
		}
		#endregion

		#region SidebarBackground
		public static DependencyProperty SidebarBackgroundProperty = DependencyProperty.Register(
			"SidebarBackground", typeof(Brush), typeof(SidebarFrame), 
			new PropertyMetadata(Application.Current.Resources["PhoneChromeBrush"]));

		/// <summary>
		/// Gets or sets the text color used for the header text
		/// </summary>
		public Brush SidebarBackground
		{
			get { return GetValue(SidebarBackgroundProperty) as Brush; }
			set { SetValue(SidebarBackgroundProperty, value); }
		}
		#endregion

		#region IsOpen Property
		public static DependencyProperty IsOpenProperty = DependencyProperty.Register(
			"IsOpen", typeof(bool), typeof(SidebarFrame), new PropertyMetadata(false, OnIsOpenPropertyChanged));

		/// <summary>
		/// Gets or sets the IsOpen property (it opens/closes the sidebar)
		/// </summary>
		public bool IsOpen
		{
			get { return (bool)GetValue(IsOpenProperty); }
			set { SetValue(IsOpenProperty, value); }
		}
		
		private static void OnIsOpenPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			SidebarFrame control = d as SidebarFrame;

			if (control != null)
			{
				control.OnIsOpenChanged(e);
			}
		}
		#endregion

		#region IsEnabled Property
		public static DependencyProperty IsMenuEnabledProperty = DependencyProperty.Register(
			"IsEnabled", typeof(bool), typeof(SidebarFrame), new PropertyMetadata(true, OnIsMenuEnabledPropertyChanged));

		public bool IsMenuEnabled
		{
			get { return (bool)GetValue(IsMenuEnabledProperty); }
			set { SetValue(IsMenuEnabledProperty, value); }
		}

		private static void OnIsMenuEnabledPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			SidebarFrame control = d as SidebarFrame;

			if (control != null)
			{
				control.OnIsMenuEnabledChanged(e);
			}
		}
		#endregion

		#endregion


		public SidebarFrame()
		{
			DefaultStyleKey = typeof(SidebarFrame);

			BackKeyPress += SidebarFrame_BackKeyPress;
		}

		void SidebarFrame_BackKeyPress(object sender, CancelEventArgs e)
		{
			if (IsOpen)
			{
				e.Cancel = true;
				IsOpen = false;
			}
		}

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();

			if (_container != null)
			{
				_container.ManipulationStarted -= OnManipulationStarted;
				_container.ManipulationCompleted -= OnManipulationCompleted;
				_container.ManipulationDelta -= OnManipulationDelta;
			}

			_container = GetTemplateChild("PART_Container") as Grid;

			if (_container != null)
			{
				_container.ManipulationStarted += OnManipulationStarted;
				_container.ManipulationCompleted += OnManipulationCompleted;
				_container.ManipulationDelta += OnManipulationDelta;
			}

			_containerTransform = GetTemplateChild("PART_ContainerTransform") as CompositeTransform;

			_sidebarContainer = GetTemplateChild("PART_SidebarContainer") as Grid;
			_sidebarContainerTransform = GetTemplateChild("PART_SidebarContainerTransform") as CompositeTransform;

            _clientArea = GetTemplateChild("PART_ClientArea") as FrameworkElement;
		}

		private void OnIsOpenChanged(DependencyPropertyChangedEventArgs e)
		{
			bool newValue = (bool)e.NewValue;
			if (newValue && IsMenuEnabled)
			{
				OpenSidebar();
			}
			else
			{
				CloseSidebar();
			}
		}

		private void OnIsMenuEnabledChanged(DependencyPropertyChangedEventArgs e)
		{
			bool newValue = (bool)e.NewValue;
			if (newValue && IsOpen)
			{
				OpenSidebar();
			}
			else
			{
				CloseSidebar();
			}
		}


		#region Event-Handlers for dragging event
		private bool _isManipulate = false;
		private double _startTranslateX = 0;
		private bool _isOpenManipulation = false;

		private void OnManipulationDelta(object sender, System.Windows.Input.ManipulationDeltaEventArgs e)
		{
			if (!IsMenuEnabled)
				return;

			if (_containerTransform == null)
				return;

			var dx = e.CumulativeManipulation.Translation.X;
			if (!_isManipulate)
			{
				_isManipulate = dx != 0;
				if (_isManipulate)
				{
					_startTranslateX = _containerTransform.TranslateX;

					var bttn = e.ManipulationContainer.FindInParents<Button>();
					if (bttn != null)
						bttn.ReleaseMouseCapture();

                    TextBoxExtensions.HideKeyboard();
				}
			}

			if (_isManipulate)
			{
				_isOpenManipulation = e.DeltaManipulation.Translation.X > 0;


				const double minScale = 0.9;
				const double maxScale = 1.0;


				double translateX = _startTranslateX + dx;
				double v = translateX / SIDEBAR_WIDTH;
				if (v < 0) v = 0;
				else if (v > 1) v = 1;


				_containerTransform.TranslateX = SIDEBAR_WIDTH * v;

				double scale = minScale + (maxScale - minScale) * v;
				_sidebarContainerTransform.ScaleX = scale;
				_sidebarContainerTransform.ScaleY = scale;

				_sidebarContainer.Opacity = v;

				_clientArea.IsHitTestVisible = v <= 0;

				if (v > 0)
				{
					if (!_isAppBarLocked)
					{
						_isAppBarLocked = true;
						ApplicationBarLocker.Lock();
					}
				}
				else
				{
					if (_isAppBarLocked)
					{
						_isAppBarLocked = false;
						ApplicationBarLocker.Unlock();
					}
				}


				if (v <= 0)
				{
					IsOpen = false;
				}
				else if (v >= 1)
				{
					IsOpen = true;
				}
			}
		}

		private void OnManipulationCompleted(object sender, System.Windows.Input.ManipulationCompletedEventArgs e)
		{
			if (_isManipulate)
			{
				_isManipulate = false;

				//Point transformedTranslation = GetTransformNoTranslation(_containerTransform).Transform(e.TotalManipulation.Translation);

				if (IsOpen != _isOpenManipulation)
				{
					IsOpen = _isOpenManipulation;
				}
				else
				{
					if (_isOpenManipulation)
						OpenSidebar();
					else
						CloseSidebar();
				}

				//if (transformedTranslation.X > 0)
				//{
				//	IsOpen = true;
				//}
				//else if (transformedTranslation.X < 0)
				//{
				//	IsOpen = false;
				//}
			}
		}

		private void OnManipulationStarted(object sender, System.Windows.Input.ManipulationStartedEventArgs e)
		{
			// TODO: Fix it!
			//if (e.Handled)
			//	e.Complete();
		}
		#endregion

		/// <summary>
		/// Opens the sidebar (goes to the according visual state)
		/// </summary>
		private void OpenSidebar()
		{
			if (!_isAppBarLocked)
			{
				_isAppBarLocked = true;
				ApplicationBarLocker.Lock();
			}

            TextBoxExtensions.HideKeyboard();

			//VisualStateManager.GoToState(this, "SidebarOpenState", true);
			this.GoToVisualState("SidebarStateGroup", "SidebarOpenState");
		}

		/// <summary>
		/// Closes the sidebar (goes to the according visual state)
		/// </summary>
		private void CloseSidebar()
		{
			//VisualStateManager.GoToState(this, "SidebarClosedState", true);
			this.GoToVisualState("SidebarStateGroup", "SidebarClosedState", (state) =>
				{
					if (_isAppBarLocked)
					{
						_isAppBarLocked = false;
						ApplicationBarLocker.Unlock();
					}
				});

			/*DispatcherTimerExtensions.SingleShot(TimeSpan.FromMilliseconds(200), (s) =>
				{
					if (_isAppBarLocked)
					{
						_isAppBarLocked = false;
						ApplicationBarLocker.Unlock();
					}
				},
				null);*/
		}

		#region Helpers
		private GeneralTransform GetTransformNoTranslation(CompositeTransform transform)
		{
			var result = new CompositeTransform();
			result.Rotation = transform.Rotation;
			result.ScaleX = transform.ScaleX;
			result.ScaleY = transform.ScaleY;
			result.CenterX = transform.CenterX;
			result.CenterY = transform.CenterY;
			result.TranslateX = 0;
			result.TranslateY = 0;
			
			return result;
		}
		#endregion
	}
}
