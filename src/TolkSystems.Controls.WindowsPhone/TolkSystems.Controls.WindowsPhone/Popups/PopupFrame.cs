﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;

namespace TolkSystems.Controls.WindowsPhone
{
	[TemplatePart(Name = "PART_PopupContainer", Type = typeof(Grid))]
	public class PopupFrame : RadPhoneApplicationFrame, IContentChanged
	{
		private Grid _popupContainer;


		public event RoutedPropertyChangedEventHandler ContentChanged;

		protected override void OnContentChanged(object oldContent, object newContent)
		{
			base.OnContentChanged(oldContent, newContent);

			if (ContentChanged != null)
				ContentChanged(this, new RoutedPropertyChangedEventArgs(oldContent, newContent));
		}


		public static DependencyProperty PopupProperty = DependencyProperty.Register("Popup",
			typeof(IPopup), typeof(PopupFrame), new PropertyMetadata(null, OnPopupPropertyChanged));

		public IPopup Popup
		{
			get { return (IPopup)GetValue(PopupProperty); }
			set { SetValue(PopupProperty, value); }
		}

		private static void OnPopupPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var sender = (PopupFrame)d;
			sender.OnPopupChanged(sender, e);
		}


		public PopupFrame()
		{
			DefaultStyleKey = typeof(PopupFrame);
		}

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();

			_popupContainer = GetTemplateChild("PART_PopupContainer") as Grid;
			if (Popup != null)
			{
				if (_popupContainer != null)
				{
					var element = Popup.GetElement();
					_popupContainer.Children.Add(element);
					VisualStateManager.GoToState(this, "ShowPopupState", true);
				}
			}
		}

		protected virtual void OnPopupChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			if (_popupContainer == null)
				return;

			var oldValue = (IPopup)e.OldValue;
			var newValue = (IPopup)e.NewValue;

			if (oldValue != null)
			{
				var element = oldValue.GetElement();
				_popupContainer.Children.Remove(element);
			}

			if (newValue != null)
			{
				var element = newValue.GetElement();
				_popupContainer.Children.Add(element);
			}

			VisualStateManager.GoToState(this, newValue != null ? "ShowPopupState" : "ClosePopupState", true);
		}
	}
}
