﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TolkSystems.Controls.WindowsPhone
{
	[Flags]
	public enum MessageBoxButtons
	{
		None        = 0,
		Ok          = 1,
		Cancel      = 2,
		Yes         = 4,
		No          = 8,
		Abort       = 16,
		Retry       = 32,
		Later       = 64,
		Never       = 128,
        Send        = 256,
        Close       = 512,

		OkCancel    = Ok | Cancel,
		YesNo       = Yes | No
	}

	public enum MessageBoxResult
	{
		None = MessageBoxButtons.None,
		Ok = MessageBoxButtons.Ok,
		Cancel = MessageBoxButtons.Cancel,
		Yes = MessageBoxButtons.Yes,
		No = MessageBoxButtons.No,
		Abort = MessageBoxButtons.Abort,
		Retry = MessageBoxButtons.Retry,
		Later = MessageBoxButtons.Later,
		Never = MessageBoxButtons.Never,
        Send = MessageBoxButtons.Send,
        Close = MessageBoxButtons.Close
	}
}
