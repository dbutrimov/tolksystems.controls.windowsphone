﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TolkSystems.Controls.WindowsPhone
{
    public class SimplePopupBox : PopupBoxBase
    {
        public SimplePopupBox()
        {
            DefaultStyleKey = typeof(SimplePopupBox);
        }
    }
}
