﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace TolkSystems.Controls.WindowsPhone
{
	public interface IPopup
	{
		event EventHandler Closed;

		void Show();
		void Close();

		FrameworkElement GetElement();
	}
}
