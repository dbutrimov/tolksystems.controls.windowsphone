﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.ComponentModel;
using System.Threading;
using System.Windows.Threading;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;
using TolkSystems.Controls.WindowsPhone.Resources;
using System.Windows.Navigation;

namespace TolkSystems.Controls.WindowsPhone
{
    public class MessageBox : PopupBoxBase
    {
        private const string TitleTextBlockName = "TitleTextBlock";
        private const string ButtonsLayoutName = "ButtonsLayout";
        private const string ScrollViewerName = "ScrollViewer";
        private const string MessageCheckBoxName = "MessageCheckBox";


        private Panel _rootLayout = null;
        private ScrollViewer _scrollViewer = null;
        private Panel _messageLayout = null;
        private TextBlock _titleTextBlock = null;
        private Grid _buttonsLayout = null;
        private CheckBox _messageCheckBox = null;


        #region Buttons DependencyProperty
        public MessageBoxButtons Buttons
		{
            get { return (MessageBoxButtons)GetValue(ButtonsProperty); }
            set { SetValue(ButtonsProperty, value); }
		}

        public static readonly DependencyProperty ButtonsProperty = DependencyProperty.RegisterAttached("Buttons",
            typeof(MessageBoxButtons), typeof(MessageBox), new PropertyMetadata(MessageBoxButtons.None, OnButtonsPropertyChanged));

        private static void OnButtonsPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            var messageBox = (MessageBox)sender;
            if (messageBox._buttonsLayout != null)
                messageBox.UpdateButtons(messageBox.Buttons);
        }
        #endregion

        #region Title DependencyProperty
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public static readonly DependencyProperty TitleProperty = DependencyProperty.RegisterAttached("Title",
            typeof(string), typeof(MessageBox), new PropertyMetadata(null, OnTitlePropertyChanged));

        private static void OnTitlePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            var messageBox = (MessageBox)sender;
            if (messageBox._titleTextBlock != null)
                messageBox._titleTextBlock.Text = messageBox.Title;
        }
        #endregion

        #region ShowCheckBox DependencyProperty
        public bool ShowCheckBox
        {
            get { return (bool)GetValue(ShowCheckBoxProperty); }
            set { SetValue(ShowCheckBoxProperty, value); }
        }

        public static readonly DependencyProperty ShowCheckBoxProperty = DependencyProperty.RegisterAttached("ShowCheckBox",
            typeof(bool), typeof(MessageBox), new PropertyMetadata(false, OnShowCheckBoxPropertyChanged));

        private static void OnShowCheckBoxPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            var messageBox = (MessageBox)sender;
            if (messageBox._messageCheckBox != null)
                messageBox._messageCheckBox.Visibility = messageBox.ShowCheckBox ? Visibility.Visible : Visibility.Collapsed;
        }
        #endregion

        #region IsChecked DependencyProperty
        public bool? IsChecked
        {
            get { return (bool?)GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }

        public static readonly DependencyProperty IsCheckedProperty = DependencyProperty.RegisterAttached("IsChecked",
            typeof(bool?), typeof(MessageBox), new PropertyMetadata(false, OnIsCheckedPropertyChanged));

        private static void OnIsCheckedPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            var messageBox = (MessageBox)sender;
            if (messageBox._messageCheckBox != null)
                messageBox._messageCheckBox.IsChecked = messageBox.IsChecked;
        }
        #endregion

        public MessageBoxResult Result { get; protected set; }

        #region CheckBoxContent DependencyProperty
        public object CheckBoxContent
        {
            get { return (object)GetValue(CheckBoxContentProperty); }
            set { SetValue(CheckBoxContentProperty, value); }
        }

        public static readonly DependencyProperty CheckBoxContentProperty = DependencyProperty.RegisterAttached("CheckBoxContent",
            typeof(object), typeof(MessageBox), new PropertyMetadata(null, OnCheckBoxContentPropertyChanged));

        private static void OnCheckBoxContentPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            var messageBox = (MessageBox)sender;
            if (messageBox._messageCheckBox != null)
                messageBox._messageCheckBox.Content = messageBox.CheckBoxContent;
        }
        #endregion



        public MessageBox() 
			: base()
        {
            DefaultStyleKey = typeof(MessageBox);
        }

        #region OnApplyTemplate
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _rootLayout = GetTemplateChild("RootLayout") as Panel;
            _messageLayout = GetTemplateChild("MessageLayout") as Panel;
            _scrollViewer = GetTemplateChild(ScrollViewerName) as ScrollViewer;
            _titleTextBlock = GetTemplateChild(TitleTextBlockName) as TextBlock;
            _buttonsLayout = GetTemplateChild(ButtonsLayoutName) as Grid;
            _messageCheckBox = GetTemplateChild(MessageCheckBoxName) as CheckBox;

            _titleTextBlock.Text = Title;
            UpdateButtons(Buttons);

            //_scrollViewer.SizeChanged += MessageLayout_SizeChanged;
            _messageLayout.SizeChanged += MessageLayout_SizeChanged;
            MessageLayout_SizeChanged(null, null);

            _messageCheckBox.Visibility = ShowCheckBox ? Visibility.Visible : Visibility.Collapsed;
            _messageCheckBox.IsChecked = IsChecked;
            _messageCheckBox.Content = CheckBoxContent;

            //if (_isShowed && _rootLayout != null)
            //    ControlsHelper.GoToVisualState(_rootLayout, "MessageStates", "ShowMessage");
        }
        #endregion

        private void MessageLayout_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var scrollViewHeight = _scrollViewer.ActualHeight;
            scrollViewHeight -= _scrollViewer.Padding.Top + _scrollViewer.Padding.Bottom;

            _scrollViewer.VerticalScrollBarVisibility = (_messageLayout.ActualHeight > scrollViewHeight) ?
                ScrollBarVisibility.Auto : ScrollBarVisibility.Disabled;
        }

		//protected override void OnNavigatingBack(object sender, NavigatingCancelEventArgs e)
		//{
		//	base.OnNavigatingBack(sender, e);

		//	T result = (T)Activator.CreateInstance(typeof(T));
		//	result.Result = MessageBoxResult.None;
		//	result.IsChecked = IsChecked;
		//	OnResult(result);
		//}
   

        private void ButtonClickedHandler(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            Result = (MessageBoxResult)button.Tag;

            Close();
        }


        protected void UpdateButtons(MessageBoxButtons buttons)
        {
            foreach (Button button in _buttonsLayout.Children)
                button.Click -= ButtonClickedHandler;

            _buttonsLayout.Children.Clear();
            _buttonsLayout.ColumnDefinitions.Clear();

            int count = 0;
            for (int i = 0; i < 16; i++)
            {
                int n = (int)Math.Pow(2, i);
                MessageBoxButtons current = (MessageBoxButtons)n;

                if ((buttons & current) == current)
                {
                    Button b = new Button();
                    b.Click += new RoutedEventHandler(ButtonClickedHandler);
                    b.Tag = n;

                    var resName = Enum.GetName(typeof(MessageBoxButtons), current);
                    var content = AppResources.ResourceManager.GetString(resName);
                    b.Content = content;

                    _buttonsLayout.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                    _buttonsLayout.Children.Add(b);
                    Grid.SetColumn(b, _buttonsLayout.ColumnDefinitions.Count - 1);

                    count++;
                }
            }

            if (count <= 1)
            {
                _buttonsLayout.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            
                foreach (FrameworkElement item in _buttonsLayout.Children)
                    Grid.SetColumn(item, _buttonsLayout.ColumnDefinitions.Count - 1);
            }
        }



        public static MessageBox Show(string message, string title, MessageBoxButtons buttons = MessageBoxButtons.Ok)
        {
            var messageBox = new MessageBox();
            messageBox.Title = title;
            messageBox.Content = message;
            messageBox.Buttons = buttons;

            PopupQueue.Add(messageBox);
            return messageBox;
        }
    }
}
