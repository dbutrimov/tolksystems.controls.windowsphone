﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TolkSystems.Controls.WindowsPhone
{
	public static class PopupQueue
	{
		private static List<IPopup> _queue = new List<IPopup>();

		public static void Add(IPopup popup)
		{
			if (popup == null)
				throw new ArgumentNullException("popup");

			_queue.Add(popup);
			if (_queue.Count <= 1)
				ShowNext();
		}

		public static bool Remove(IPopup popup)
		{
			if (popup == null)
				throw new ArgumentNullException("popup");

			var index = _queue.IndexOf(popup);
			if (index < 0)
				return false;

			_queue.RemoveAt(index);
			if (index <= 0)
				ShowNext();

			return true;
		}

		private static bool ShowNext()
		{
			var next = _queue.FirstOrDefault();
			if (next != null)
			{
				next.Closed += Popup_Closed;
				next.Show();
			}

			return false;
		}

		private static void Popup_Closed(object sender, EventArgs e)
		{
			var control = (IPopup)sender;

			var index = _queue.IndexOf(control);
			if (index < 0)
				return;

			_queue.RemoveAt(index);
			if (index <= 0)
				ShowNext();
		}
	}
}
