﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Collections.Generic;
using Microsoft.Phone.Shell;
using System.Windows.Navigation;
using System.ComponentModel;
using System.Windows.Threading;
using System.Windows.Data;
using System.Diagnostics;

namespace TolkSystems.Controls.WindowsPhone
{
    public abstract class PopupBoxBase : ContentControl, IPopup
    {
		private PopupFrame _frame;
		private bool _isShown;

		public event EventHandler Closed;
        protected virtual void OnClosed(EventArgs e)
		{
			if (Closed != null)
				Closed(this, e);
		}

        public PopupBoxBase()
        {
			this.RegisterForNotification("ContentTemplate", OnContentTemplateChanged);
			this.RegisterForNotification("Template", OnTemplateChanged);
        }

		public FrameworkElement GetElement()
		{
			return this;
		}

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            if (_isShown)
            {
                this.GoToVisualState("PopupStateGroup", "PopupVisibleState");
                //VisualStateManager.GoToState(this, "PopupVisibleState", true);
            }
        }

		private void Frame_BackKeyPress(object sender, CancelEventArgs e)
		{
			e.Cancel = true;
			Close();
		}

        #region Show
        public void Show()
        {
			if (_isShown)
				return;

			_isShown = true;
			
			ApplicationBarLocker.Lock();

            OnApplicationBarLocked();
            //DispatcherTimerExtensions.SingleShot(TimeSpan.FromMilliseconds(200), (s) => OnApplicationBarLocked(), null);
        }

        private void OnApplicationBarLocked()
        {
            var app = (IFrameApplication)Application.Current;
            _frame = (PopupFrame)app.RootFrame;
            _frame.BackKeyPress += Frame_BackKeyPress;
            _frame.Popup = this;

            this.GoToVisualState("PopupStateGroup", "PopupVisibleState");
            //VisualStateManager.GoToState(this, "PopupVisibleState", true);
        }
        #endregion

        #region Close
        public void Close()
        {
			if (!_isShown)
				return;

            if (!this.GoToVisualState("PopupStateGroup", "PopupCollapsedState", (s) => OnClosed()))
                OnClosed();

			//VisualStateManager.GoToState(this, "PopupCollapsedState", true);

            //DispatcherTimerExtensions.SingleShot(TimeSpan.FromMilliseconds(200), OnClosed, null);
        }

        private void OnClosed()
        {
            if (_frame.Popup == this)
                _frame.Popup = null;

            _frame.BackKeyPress -= Frame_BackKeyPress;
            _frame = null;

            ApplicationBarLocker.Unlock();

            _isShown = false;

            OnClosed(EventArgs.Empty);
        }
        #endregion


		protected virtual void OnContentTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

		protected virtual void OnTemplateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }
    }
}
