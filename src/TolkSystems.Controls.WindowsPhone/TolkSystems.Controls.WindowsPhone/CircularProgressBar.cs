﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace TolkSystems.Controls.WindowsPhone
{
	public class CircularProgressBar : Control
	{
		private const string PATH_ROOT_NAME = "PathRoot";
		private const string PATH_FIGURE_NAME = "PathFigure";
		private const string ARC_SEGMENT_NAME = "ArcSegment";

		private Path _pathRoot = null;
		private PathFigure _pathFigure = null;
		private ArcSegment _arcSegment = null;

		#region Angle DependencyProperty
		public double Angle
		{
			get { return (double)GetValue(AngleProperty); }
			set { SetValue(AngleProperty, value); }
		}

		public static readonly DependencyProperty AngleProperty =
			DependencyProperty.Register("Angle", typeof(double), typeof(CircularProgressBar),
			new PropertyMetadata(120d, OnAngleChanged));

		private static void OnAngleChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			var progressBar = (CircularProgressBar)obj;
			progressBar.RenderArc();
		}
		#endregion

		#region Value DependencyProperty
		public double Value
		{
			get { return (double)GetValue(ValueProperty); }
			set { SetValue(ValueProperty, value); }
		}

		public static readonly DependencyProperty ValueProperty =
			DependencyProperty.Register("Value", typeof(double), typeof(CircularProgressBar),
			new PropertyMetadata(0d, OnValueChanged));

		private static void OnValueChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			var progressBar = (CircularProgressBar)obj;
			progressBar.Angle = (progressBar.Value * 360) / 100;
		}
		#endregion

		#region Radius DependencyProperty
		public double Radius
		{
			get { return (double)GetValue(RadiusProperty); }
			set { SetValue(RadiusProperty, value); }
		}

		public static readonly DependencyProperty RadiusProperty =
			DependencyProperty.Register("Radius", typeof(double), typeof(CircularProgressBar),
			new PropertyMetadata(25d, OnRadiusChanged));

		private static void OnRadiusChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			var progressBar = (CircularProgressBar)obj;
			progressBar.RenderArc();
		}
		#endregion

		#region StrokeThickness DependencyProperty
		public double StrokeThickness
		{
			get { return (double)GetValue(StrokeThicknessProperty); }
			set { SetValue(StrokeThicknessProperty, value); }
		}

		public static readonly DependencyProperty StrokeThicknessProperty =
			DependencyProperty.Register("StrokeThickness", typeof(double), typeof(CircularProgressBar),
			new PropertyMetadata(5d, OnStrokeThicknessChanged));

		private static void OnStrokeThicknessChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{

		}
		#endregion

		#region SegmentColor DependencyProperty
		public Brush SegmentColor
		{
			get { return (Brush)GetValue(SegmentColorProperty); }
			set { SetValue(SegmentColorProperty, value); }
		}

		public static readonly DependencyProperty SegmentColorProperty =
			DependencyProperty.Register("SegmentColor", typeof(Brush), typeof(CircularProgressBar),
			new PropertyMetadata(new SolidColorBrush(Colors.Red), OnSegmentColorChanged));

		private static void OnSegmentColorChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{

		}
		#endregion


		public CircularProgressBar()
		{
			DefaultStyleKey = typeof(CircularProgressBar);

			Angle = (Value * 360) / 100;
		}

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();

			_pathRoot = (Path)GetTemplateChild(PATH_ROOT_NAME);
			_pathFigure = (PathFigure)GetTemplateChild(PATH_FIGURE_NAME);
			_arcSegment = (ArcSegment)GetTemplateChild(ARC_SEGMENT_NAME);

			RenderArc();
		}

		protected void RenderArc()
		{
			if (_pathRoot == null || _pathFigure == null || _arcSegment == null)
				return;

			Point startPoint = new Point(Radius, 0);
			Point endPoint = ComputeCartesianCoordinate(Angle, Radius);
			endPoint.X += Radius;
			endPoint.Y += Radius;

			_pathRoot.Width = Radius * 2 + StrokeThickness;
			_pathRoot.Height = Radius * 2 + StrokeThickness;
			_pathRoot.Margin = new Thickness(StrokeThickness, StrokeThickness, 0, 0);

			bool largeArc = Angle > 180.0;

			Size outerArcSize = new Size(Radius, Radius);

			_pathFigure.StartPoint = startPoint;

			if (startPoint.X == Math.Round(endPoint.X) && startPoint.Y == Math.Round(endPoint.Y))
				endPoint.X -= 0.01;

			_arcSegment.Point = endPoint;
			_arcSegment.Size = outerArcSize;
			_arcSegment.IsLargeArc = largeArc;
		}

		private Point ComputeCartesianCoordinate(double angle, double radius)
		{
			// convert to radians
			double angleRad = (Math.PI / 180.0) * (angle - 90);

			double x = radius * Math.Cos(angleRad);
			double y = radius * Math.Sin(angleRad);

			return new Point(x, y);
		}
	}
}
