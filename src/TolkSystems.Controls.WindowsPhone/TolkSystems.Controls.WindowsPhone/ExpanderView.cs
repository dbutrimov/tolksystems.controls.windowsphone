﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace TolkSystems.Controls.WindowsPhone
{
	public class ExpanderView : Control
	{

		#region Header DependencyProperty
		public object Header
		{
			get { return GetValue(HeaderProperty); }
			set { SetValue(HeaderProperty, value); }
		}

		public static readonly DependencyProperty HeaderProperty =
			DependencyProperty.Register("Header", typeof(object), typeof(ExpanderView),
			new PropertyMetadata(null, OnHeaderPropertyChanged));

		private static void OnHeaderPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			var sender = (ExpanderView)obj;

		}
		#endregion

		#region HeaderTemplate DependencyProperty
		public DataTemplate HeaderTemplate
		{
			get { return (DataTemplate)GetValue(HeaderTemplateProperty); }
			set { SetValue(HeaderTemplateProperty, value); }
		}

		public static readonly DependencyProperty HeaderTemplateProperty =
			DependencyProperty.Register("HeaderTemplate", typeof(DataTemplate), typeof(ExpanderView),
			new PropertyMetadata(null, OnHeaderTemplatePropertyChanged));

		private static void OnHeaderTemplatePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			var sender = (ExpanderView)obj;

		}
		#endregion


        #region ExpandableContent DependencyProperty
        public object ExpandableContent
		{
			get { return GetValue(ExpandableContentProperty); }
			set { SetValue(ExpandableContentProperty, value); }
		}

		public static readonly DependencyProperty ExpandableContentProperty =
			DependencyProperty.Register("ExpandableContent", typeof(object), typeof(ExpanderView),
			new PropertyMetadata(null, OnExpandableContentPropertyChanged));

		private static void OnExpandableContentPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			var sender = (ExpanderView)obj;

		}
		#endregion

        #region ExpandableContentTemplate DependencyProperty
        public DataTemplate ExpandableContentTemplate
		{
			get { return (DataTemplate)GetValue(ExpandableContentTemplateProperty); }
			set { SetValue(ExpandableContentTemplateProperty, value); }
		}

		public static readonly DependencyProperty ExpandableContentTemplateProperty =
			DependencyProperty.Register("ExpandableContentTemplate", typeof(DataTemplate), typeof(ExpanderView),
			new PropertyMetadata(null, OnExpandableContentTemplatePropertyChanged));

		private static void OnExpandableContentTemplatePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			var sender = (ExpanderView)obj;

		}
		#endregion
		

		#region IsExpanded DependencyProperty
		public bool IsExpanded
		{
			get { return (bool)GetValue(IsExpandedProperty); }
			set { SetValue(IsExpandedProperty, value); }
		}

		public static readonly DependencyProperty IsExpandedProperty =
			DependencyProperty.Register("IsExpanded", typeof(bool), typeof(ExpanderView),
			new PropertyMetadata(false, OnIsExpandedPropertyChanged));

		private static void OnIsExpandedPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			var sender = (ExpanderView)obj;
			sender.OnIsExpandedChanged();
		}
		#endregion


		#region CanExpanded DependencyProperty
		public bool CanExpanded
		{
			get { return (bool)GetValue(CanExpandedProperty); }
			set { SetValue(CanExpandedProperty, value); }
		}

		public static readonly DependencyProperty CanExpandedProperty =
			DependencyProperty.Register("CanExpanded", typeof(bool), typeof(ExpanderView),
			new PropertyMetadata(true, OnCanExpandedPropertyChanged));

		private static void OnCanExpandedPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			var sender = (ExpanderView)obj;
			sender.OnCanExpandedChanged();
		}
		#endregion


		private FrameworkElement _rootLayout = null;
		private ContentControl _headerElement = null;
		private ContentControl _expandedContentElement = null;
		private FrameworkElement _expanderPanel = null;
		

		public ExpanderView()
		{
			DefaultStyleKey = typeof(ExpanderView);

		}
        
		protected virtual void OnIsExpandedChanged()
		{
			SetExpandedContentVisibility(IsExpanded ? Visibility.Visible : Visibility.Collapsed);
		}

		private void OnCanExpandedChanged()
		{
			if (!CanExpanded)
				IsExpanded = false;
		}


		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();


			_rootLayout = GetTemplateChild("RootLayout") as FrameworkElement;


			_headerElement = GetTemplateChild("Header") as ContentControl;
			if (_headerElement != null)
			{

			}


			_expandedContentElement = GetTemplateChild("ExpandableContent") as ContentControl;
			if (_expandedContentElement != null)
			{
				SetExpandedContentVisibility(IsExpanded ? Visibility.Visible : Visibility.Collapsed);
			}


			_expanderPanel = GetTemplateChild("ExpanderPanel") as FrameworkElement;
			if (_expanderPanel != null)
			{
				_expanderPanel.Tap += (s, e) => 
				{
					if (CanExpanded)
						IsExpanded = !IsExpanded; 
				};
			}

		}


		private void SetExpandedContentVisibility(Visibility visibility)
		{
			if (visibility == _expandedContentElement.Visibility)
				return;

			_rootLayout.SizeChanged += ExpandedContent_SizeChanged_1;
			_expandedContentElement.Visibility = visibility;
		}

		private void ExpandedContent_SizeChanged_1(object sender, SizeChangedEventArgs e)
		{
			_rootLayout.SizeChanged -= ExpandedContent_SizeChanged_1;
			_rootLayout.SizeChanged += ExpandedContent_SizeChanged_2;
			_expandedContentElement.Visibility = 
				_expandedContentElement.Visibility == Visibility.Visible ?
				Visibility.Collapsed : Visibility.Visible;
		}

		private void ExpandedContent_SizeChanged_2(object sender, SizeChangedEventArgs e)
		{
			_rootLayout.SizeChanged -= ExpandedContent_SizeChanged_2;
			_rootLayout.SizeChanged += ExpandedContent_SizeChanged_3;
			_expandedContentElement.Visibility =
				_expandedContentElement.Visibility == Visibility.Visible ?
				Visibility.Collapsed : Visibility.Visible;
		}

		private void ExpandedContent_SizeChanged_3(object sender, SizeChangedEventArgs e)
		{
			_rootLayout.SizeChanged -= ExpandedContent_SizeChanged_3;
		}

	}
}
