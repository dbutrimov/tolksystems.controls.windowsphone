﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using TestApp.Resources;
using TolkSystems.Controls.WindowsPhone;
using System.Windows.Media;
using System.Diagnostics;
using TolkControls = TolkSystems.Controls.WindowsPhone;

namespace TestApp
{
	public partial class MainPage : ApplicationPage
	{
		// Constructor
		public MainPage()
		{
			InitializeComponent();
			BuildLocalizedApplicationBar();

			var items = new List<Brush>();
			for (int i = 0; i < 40; i++)
				items.Add(new SolidColorBrush(i % 2 != 0 ? Colors.Blue : Colors.Red));

			ListView.ItemsSource = items;

			// Sample code to localize the ApplicationBar
			//BuildLocalizedApplicationBar();
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			//var app = (IFrameApplication)Application.Current;
			//var frame = (SidebarFrame)app.RootFrame;
			//frame.IsMenuEnabled = !frame.IsMenuEnabled;

			//Debug.WriteLine("ololo");

			//TolkControls.PopupBox.Show("ololo", "trololo", MessageBoxButtons.Ok, null);

            var msg = new TolkSystems.Controls.WindowsPhone.MessageBox();
            msg.Title = "title";
            msg.Content = "message";
            msg.Buttons = MessageBoxButtons.Ok;

            msg.Show();

            //System.Windows.MessageBox.Show("message", "title", MessageBoxButton.OK);
		}

		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			base.OnNavigatedTo(e);

		}

		// Sample code for building a localized ApplicationBar
		private void BuildLocalizedApplicationBar()
		{
			// Set the page's ApplicationBar to a new instance of ApplicationBar.
			ApplicationBar = new ApplicationBar();

			// Create a new button and set the text value to the localized string from AppResources.
			ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
			appBarButton.Text = AppResources.AppBarButtonText;
			ApplicationBar.Buttons.Add(appBarButton);

			// Create a new menu item with the localized string from AppResources.
			ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
			ApplicationBar.MenuItems.Add(appBarMenuItem);
		}
	}
}